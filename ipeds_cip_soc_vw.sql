
-- CIP SOC associations
-- Bridge

-- https://www.bls.gov/soc/2018/crosswalks_used_by_agencies.htm
-- https://nces.ed.gov/ipeds/cipcode/resources.aspx?y=55

-- export SOC-CIP tab (includes Unmatched SOC codes)

-- header
-- SOCCode,SOCTitle,CIPCode,CIPTitle

truncate table ipeds_cip_soc_import;
create table ipeds_cip_soc_import (
  soccode varchar2(200) -- 7
  ,soctitle varchar2(200) -- 105
  ,cipcode varchar(200) -- 7
  ,ciptitle varchar(200) -- 100
);

select max(length(ciptitle)) from ipeds_cip_soc_import;
select soccode, cipcode, count(*) from ipeds_cip_soc_import group by soccode, cipcode having count(*) > 1;

create table ipeds_cip_soc (
  publishdate date
  ,soccode varchar2(7)
  ,soctitle varchar2(150)
  ,cipcode varchar2(6) -- store without the period
  ,ciptitle varchar2(150)
  ,primary key(soccode, cipcode)
);


create or replace view ipeds_cip_soc_load_vw as
select
  date '2020-01-01' publishdate -- SOC 2018, CIP 2020
  ,trim(soccode) soccode
  ,trim(soctitle) soctitle
  ,trim(replace(cipcode, '.', '')) cipcode
  ,trim(ciptitle) ciptitle
from ipeds_cip_soc_import;
select * from ipeds_cip_soc_load_vw;


insert into ipeds_cip_soc select * from ipeds_cip_soc_load_vw;

select * from ipeds_cip_soc i
join ipeds_cip_soc_load_vw s on s.soccode = i.soccode and s.cipcode = i.cipcode
;

select * from ipeds_cip_soc;
create or replace view ipeds_cip_soc_vw as select * from ipeds_cip_soc;

