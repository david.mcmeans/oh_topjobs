/*

Name                  Null Type          
--------------------- ---- ------------- 
PUBLISHDATE                DATE          
IPEDS_AWARDLEVEL_CODE      VARCHAR2(8)   
SORT_ORDER                 NUMBER        
ISDEGREE                   CHAR(1)       
IPEDS_AWARDLEVEL_NAME      VARCHAR2(200) 
ISACTIVE                   CHAR(1)       
NOTES                      VARCHAR2(52)  


0	Minor	N	0	01-Jan-2023
1A	Certificate (less than 9 semester hours)	N	1	01-Jan-2023
1B	Certificate (9-29 semester hours)	N	2	01-Jan-2023
2	Certificate (30-59 semester hours)	N	20	01-Jan-2023
3	Associate's degree	Y	30	01-Jan-2023
5	Bachelor's degree	Y	50	01-Jan-2023
6	Postbaccalaureate certificate	N	60	01-Jan-2023
7	Master's degree	Y	70	01-Jan-2023
8	Post-master's certificate	N	80	01-Jan-2023
17	Doctoral degree (research/scholarship)	Y	170	01-Jan-2023
18	Doctoral degree (professional practice)	Y	180	01-Jan-2023
19	Doctoral degree (other)	Y	190	01-Jan-2023
*/

REM INSERTING into EXPORT_TABLE
SET DEFINE OFF;
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'0',0,'N','Minor','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'1A',1,'N','Certificate (less than 9 semester hours)','Y','20 Aug 23: Coming soon, not yet in rep major funding');
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'1B',2,'N','Certificate (9-29 semester hours)','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'2',20,'N','Certificate (30-59 semester hours)','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'3',30,'Y','Associate''s degree','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'4',40,'N','Certificate (60 or more semester hours)','N','20 Aug 23: WSU does not offer this currently');
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'5',50,'Y','Bachelor''s degree','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'6',60,'N','Postbaccalaureate certificate','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'7',70,'Y','Master''s degree','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'8',80,'N','Post-master''s certificate','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'17',170,'Y','Doctoral degree (research/scholarship)','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'18',180,'Y','Doctoral degree (professional practice)','Y',null);
Insert into EXPORT_TABLE (PUBLISHDATE,IPEDS_AWARDLEVEL_CODE,SORT_ORDER,ISDEGREE,IPEDS_AWARDLEVEL_NAME,ISACTIVE,NOTES) values (to_date('01-Jan-2023','DD-Mon-YYYY'),'19',190,'Y','Doctoral degree (other)','Y',null);

drop table l_ipeds_awardlevels;
create table l_ipeds_awardlevels as select * from export_table;

create or replace view l_ipeds_awardlevels_vw as
select 
   ipeds_awardlevel_code
  ,ipeds_awardlevel_name
  ,isdegree
  ,sort_order
  ,publishdate
from l_ipeds_awardlevels
where isactive = 'Y'
;
select * from l_ipeds_awardlevels_vw;



