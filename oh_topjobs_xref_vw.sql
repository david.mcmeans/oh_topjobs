
-- Ohio top jobs cross reference list

-- Includes codes from Ohio top jobs PDF are combinations of existing codes given invented soccodes
-- Includes codes from 2018 BLS SOC list not at the Detailed level
-- Includes codes from BLS SOC list for 2000 and 2010

-- build xref for incorrect codes
create or replace view oh_topjobs_xref_vw as
-- oh top jobs combinations with invented soc codes
      select '51-2099' soccode, '51-2098' otj_soccode from dual --Assemblers and Fabricators, All Other, Including Team Assemblers
UNION select '51-2092' soccode, '51-2098' otj_soccode from dual --Assemblers and Fabricators, All Other, Including Team Assemblers
UNION select '51-2022' soccode, '51-2028' otj_soccode from dual --Electrical, Electronic, and Electromechanical Assemblers, Except Coil Winders, Tapers, and Finishers
UNION select '51-2023' soccode, '51-2028' otj_soccode from dual --Electrical, Electronic, and Electromechanical Assemblers, Except Coil Winders, Tapers, and Finishers
UNION select '21-1011' soccode, '21-1018' otj_soccode from dual --Substance Abuse, Behavioral Disorder, and Mental Health Counselors
UNION select '21-1014' soccode, '21-1018' otj_soccode from dual --Substance Abuse, Behavioral Disorder, and Mental Health Counselors
UNION select '13-1199' soccode, '13-1198' otj_soccode from dual --Business Operations Specialists, All Other
UNION select '29-9099' soccode, '29-9098' otj_soccode from dual --Healthcare Practitioners and Technical Workers, All Other
UNION select '15-1242' soccode, '15-1245' otj_soccode from dual --Database Administrators
UNION select '29-2099' soccode, '29-2098' otj_soccode from dual --Health Technologists and Technicians, All Other
UNION select '29-1229' soccode, '29-1228' otj_soccode from dual --Physicians and Surgeons, All Other (2010) ->Physicians, All Other
UNION select '29-1249' soccode, '29-1228' otj_soccode from dual --Physicians and Surgeons, All Other (2010) -> Surgeons, All Other
UNION select '11-9199' soccode, '11-9198' otj_soccode from dual --Managers, All Other
UNION select '53-1049' soccode, '53-1048' otj_soccode from dual --First-Line Supervisors of Transportation Workers, All Other
-- codes from 2018 not at Detailed level and codes from 2000 and 2010
UNION select '29-2011' soccode, '29-2010' otj_soccode from dual --Clinical Laboratory Technologists and Technicians->Medical and Clinical Laboratory Technologists
UNION select '29-2012' soccode, '29-2010' otj_soccode from dual --Clinical Laboratory Technologists and Technicians->Medical and Clinical Laboratory Technicians
UNION select '11-2032' soccode, '11-2031' otj_soccode from dual --Public Relations and Fundraising Managers->Public Relations Managers
UNION select '11-2033' soccode, '11-2031' otj_soccode from dual --Public Relations and Fundraising Managers->Fundraising Managers
UNION select '29-2099' soccode, '29-2054' otj_soccode from dual --Respiratory Therapy Technicians->Health Technologists and Technicians, All Other
UNION select '19-3033' soccode, '19-3031' otj_soccode from dual --Clinical, Counseling, and School Psychologists->Clinical and Counseling Psychologists
UNION select '31-1133' soccode, '31-1013' otj_soccode from dual --Psychiatric Aides->Psychiatric Aides
UNION select '31-1122' soccode, '39-9021' otj_soccode from dual --Personal Care Aides->Personal Care Aides
UNION select '29-2042' soccode, '29-2041' otj_soccode from dual --Emergency Medical Technicians and Paramedics->Emergency Medical Technicians
UNION select '29-2043' soccode, '29-2041' otj_soccode from dual --Emergency Medical Technicians and Paramedics->Paramedics
UNION select '47-5023' soccode, '47-5021' otj_soccode from dual --Earth Drillers, Except Oil and Gas->Earth Drillers, Except Oil and Gas
UNION select '53-3051' soccode, '53-3022' otj_soccode from dual --Bus Drivers, School->Bus Drivers, School
UNION select '31-1131' soccode, '31-1014' otj_soccode from dual --Nursing Assistants->Nursing Assistants
UNION select '39-1022' soccode, '39-1021' otj_soccode from dual --First-Line Supervisors of Personal Service Workers->First-Line Supervisors of Personal Service Workers
UNION select '11-3012' soccode, '11-3011' otj_soccode from dual --Administrative Services Managers->Administrative Services Managers
UNION select '15-1252' soccode, '15-1132' otj_soccode from dual --Software Developers, Applications->Software Developers
UNION select '25-2055' soccode, '25-2052' otj_soccode from dual --Special Education Teachers, Kindergarten and Elementary School->Special Education Teachers, Kindergarten
UNION select '25-2056' soccode, '25-2052' otj_soccode from dual --Special Education Teachers, Kindergarten and Elementary School->Special Education Teachers, Elementary School
UNION select '25-9042' soccode, '25-9041' otj_soccode from dual --Teacher Assistants->Teaching Assistants, Preschool, Elementary, Middle, and Secondary School, Except Special Education
UNION select '25-9043' soccode, '25-9041' otj_soccode from dual --Teacher Assistants->Teaching Assistants, Special Education
UNION select '25-9044' soccode, '25-9041' otj_soccode from dual --Teacher Assistants->Teaching Assistants, Postsecondary
UNION select '25-9049' soccode, '25-9041' otj_soccode from dual --Teacher Assistants->Teaching Assistants, All Other
UNION select '29-2072' soccode, '29-2071' otj_soccode from dual --Medical Records and Health Information Technicians->Medical Records Specialists
UNION select '51-9124' soccode, '51-9121' otj_soccode from dual --Coating, Painting, and Spraying Machine Setters, Operators, and Tenders->Coating, Painting, and Spraying Machine Setters, Operators, and Tenders
UNION select '15-1252' soccode, '15-1133' otj_soccode from dual --Software Developers, Systems Software->Software Developers
UNION select '15-1254' soccode, '15-1134' otj_soccode from dual --Web Developers->Web Developers
UNION select '31-1121' soccode, '31-1011' otj_soccode from dual --Home Health Aides->Home Health Aides

;
select * from oh_topjobs_xref_vw;


select publishdate, soccode, soctitle, socgroup, soclevel, socdefinition from bls_soc_base_vw
where soccode in (
select t.soccode from oh_topjobs_base_vw t
left join bls_soc_vw s on s.soccode = t.soccode
where  s.soccode is null
)
;

select * from bls_soc_base_vw where publishdate = date '2018-01-01' and soctitle like '%Home%'; -- and soccode like '29-2%';
select * from bls_soc_base_vw where publishdate = date '2018-01-01' and soccode like '53-3%';

select * from bls_soc_base_vw where soccode like '15-12%' and publishdate = date '2018-01-01';
select * from oh_topjobs_vw where soccode = '15-1133'; -- 29-2011 29-2012

