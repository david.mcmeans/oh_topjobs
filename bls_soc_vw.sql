
-- Bureau of Labor and Statistics (BLS) / Standard Occupational Classification (SOC) codes

-- Publish date
--   January 2000, January, 2010, January 2018

truncate table bls_soc_import;
drop table bls_soc_import;
create table bls_soc_import (
   socgroup varchar2(200) -- Detailed Major Broad Minor
  ,soccode varchar2(200) -- max is 7
  ,soctitle varchar2(200) -- max is 105
  ,socdefinition varchar2(4000) -- max is 828
);
select * from bls_soc_import;
select soccode, count(*) from bls_soc_import group by soccode having count(*) > 1;
commit;

--drop table bls_soc;
create table bls_soc (
   publishdate date
  ,socgroup varchar2(10)
  ,soccode varchar2(7)
  ,soctitle varchar2(150) 
  ,socdefinition varchar2(1000) 
  ,primary key (publishdate, soccode)
);

select * from bls_soc where soctitle like 'Database%';
select * from bls_soc where to_char(publishdate, 'YYYY') = '2010';

create or replace view bls_soc_load_vw as
select
  date '2010-02-01' publishdate -- update to match incoming data
  ,socgroup
  ,trim(soccode) soccode
  ,trim(soctitle) soctitle
  ,trim(socdefinition) socdefinition
from bls_soc_import
where 1=1
and soccode is not null
;
select * from bls_soc_load_vw;
insert into bls_soc select * from bls_soc_load_vw;
commit;




-- Ohio top jobs as 15-1245 for Database Administrators
-- 15-1061 (2000) 15-1141 (2010) 15-1242 (2018)
select * from bls_soc where soctitle like 'Database Administrator%' order by publishdate desc;

create or replace view bls_soc_base_vw as
select
  soccode
  ,soctitle
  ,socgroup
  ,soclevel
  -- SOC indented according to level (group)
  ,lpad(' ', (nvl(soclevel,1)-1)*2) || soccode soccode_fm
  ,socdefinition
  ,publishdate
from (  
select 
  soccode
  ,soctitle
  ,socgroup
  ,case when socgroup = 'Major' then 1
        when socgroup = 'Minor' then 2
        when socgroup = 'Broad' then 3
        when socgroup = 'Detailed' then 4
   end soclevel     
  ,socdefinition
  ,publishdate
from bls_soc
--where publishdate = date '2018-01-01'
) a
;
select * from bls_soc_base_vw;

select distinct publishdate from bls_soc_base_vw;
select * from bls_soc_base_vw where publishdate = date '2000-01-01';
select * from bls_soc_base_vw where publishdate = date '2010-02-01';
select * from bls_soc_base_vw where publishdate = date '2018-01-01';

create or replace view bls_soc_vw as
select
  soccode
  ,soctitle
--  ,socgroup
--  ,soclevel
--  ,soccode_fm
  ,socdefinition
  ,publishdate
from bls_soc_base_vw
where socgroup = 'Detailed'
and publishdate = date '2018-01-01' -- latest publish date
;
select * from bls_soc_vw;

select * from bls_soc_vw where soccode like '11%';
select distinct socgroup from bls_soc_vw;
-- Major, Minor, Broad, Detailed

-- update bls_soc set publishdate = date '2018-01-01' where publishdate is null;

--

select max(length(soctitle)) from bls_soc;
select distinct socgroup from bls_soc; -- Detailed Major Broad Minor
select * from bls_soc;
select soccode, count(*) from bls_soc group by soccode having count(*) > 1;