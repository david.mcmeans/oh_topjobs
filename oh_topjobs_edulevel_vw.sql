
-- education levels from Ohio top jobs PDF and CSV file mapped to IPEDS award levels
create or replace view oh_topjobs_edulevel_vw as
      select '5'   ipeds_awardlevel_code, 'Bachelor''s degree' educationlevel from dual
union select '3'   ipeds_awardlevel_code, 'Associate''s degree' educationlevel from dual
union select '7'   ipeds_awardlevel_code, 'Master''s degree' educationlevel from dual
union select '17'  ipeds_awardlevel_code, 'Doctoral or professional degree' educationlevel from dual
union select '18'  ipeds_awardlevel_code, 'Doctoral or professional degree' educationlevel from dual
union select '19'  ipeds_awardlevel_code, 'Doctoral or professional degree' educationlevel from dual
union select 'N/A' ipeds_awardlevel_code, 'Certificate/ Some college' educationlevel from dual
union select 'N/A' ipeds_awardlevel_code, 'High school diploma or equivalent' educationlevel from dual -- not an IPEDS awward level
union select 'N/A' ipeds_awardlevel_code, 'Less than high school' educationlevel from dual -- not an IPEDS awward level
union select 'N/A' ipeds_awardlevel_code, 'No formal educational credential' educationlevel from dual -- not an IPEDS awward level
union select 'N/A' ipeds_awardlevel_code, 'Some college, no degree' educationlevel from dual -- not an IPEDS awward level
union select 'N/A' ipeds_awardlevel_code, 'Postsecondary non-degree award' educationlevel from dual -- not an IPEDS awward level
;
select * from oh_topjobs_edulevel_vw;

