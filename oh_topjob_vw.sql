
-- Ohio top jobs

--https://topjobs.ohio.gov/top-jobs-list/ohios-top-jobs-list


-- Update cadence
--   Weekly, monthly, annually? PDF publish is 25 Jan 2022

-- PDF
-- Column header
-- RefNo,ExtSOC,SOC,OccupationTitle,JobFamily,EducationLevel,OJTTraining,RelevantWorkExperience,StartingWage,MedianWage,AnnualOpenings,AnnualGrowth,Base,JobsOhio,secondTier,Critical

-- CSV import button at bottom of the table
--01. SOC,02. Title,03. Typical Education Required,04. Region,05. Preferred Experience,06. Job Category,07. Median Salary,08. Growth,09. Employment,10. Openings
--SOC,Title,TypicalEducationRequired,Region,PreferredExperience,JobCategory,MedianSalary,Growth,Employment,Openings

-- Import the CSV using this table
drop table oh_topjobs_csvimport;
create table oh_topjobs_csvimport (
  soc varchar2(20)
  ,title varchar2(200)
  ,typicaleducationrequired varchar2(200)
  ,region varchar2(200)
  ,preferredexperience varchar2(50)
  ,jobcategory varchar2(200)
  ,mediansalary varchar2(50)
  ,growth varchar2(20)
  ,employment varchar2(200)
  ,openings varchar2(20)
);  
select * from oh_topjobs_csvimport;

create or replace view oh_topjobs_csv_vw as
select 
  soc soccode
  ,title jobtitle
  ,typicaleducationrequired educationlevel
  ,region
  ,preferredexperience
  ,jobcategory
  ,mediansalary medianwage
  ,growth annualgrowth
  ,employment 
  ,openings annualopenings
from oh_topjobs_csvimport
;
select * from oh_topjobs_csv_vw;

-- statewide topjobs list.pdf
-- Use Adobe Acrobat to export PDF as a spreadsheet
-- Import the spreadsheet saved as CSV using this table
-- PDF has UTF8 characters (checkmark, dash)
drop table oh_topjobs_import;
create table oh_topjobs_import (
  refno int
  ,extsoc varchar2(20)
  ,soc varchar2(20)
  ,occupationtitle varchar2(200)
  ,jobfamily varchar2(200)
  ,educationlevel varchar2(200)
  ,ojttraining varchar2(200)
  ,relevantworkexperience varchar2(50)
  ,startingwage varchar2(50)
  ,medianwage varchar2(50)
  ,annualopenings varchar2(20)
  ,annualgrowth varchar2(20)
  ,base varchar2(3)
  ,jobsohio varchar2(3)
  ,secondtier varchar2(3)
  ,critical varchar2(3)
);  
select * from oh_topjobs_import;

create or replace view oh_topjobs_load_vw as
select
  date '2022-01-25' publishdate -- PDF has this publication date
--  ,cast(refno as int) refnum
  -- extsoc is the same as soc
--  ,replace(extsoc,'‐','-') extsoc 
  ,replace(soc,'‐','-') soccode
  ,replace(regexp_replace(occupationtitle,'[⁴]',''),'‐','-') jobtitle
  
  -- make jobcats match CSV file
  ,case when jobfamily = 'Education, Training, & Library Occupations'
    then 'Educational Instruction and Library Occupations' else replace(jobfamily, '&', 'and') end jobcategory
  ,educationlevel
  ,replace(ojttraining,'‐','-') ojttraining
  ,relevantworkexperience

    -- is string numeric?
--  ,LENGTH(TRIM(TRANSLATE(case when startingwage = '‐' then null else replace(replace(startingwage,',',''),'$','') end, ' +-.0123456789',' '))) isnumber_sw

  ,case when startingwage = '‐' then null else cast(regexp_replace(startingwage,'[,$*]','') as int) end startingwage
  --,startingwage startingwagestr
  ,case when medianwage = '‐' then null else cast(regexp_replace(medianwage,'[,$*]','') as int) end medianwage
  --,medianwage medianwagestr
  
  ,cast(replace(replace(annualopenings,',',''),'‐','-') as int) annualopenings
  ,cast(replace(replace(annualgrowth,',',''),'‐','-') as int) annualgrowth
  --,annualgrowth annualgrowthstr
  ,case when base='√' then 'Y' else 'N' end isbase
  ,case when jobsohio='√' then 'Y' else 'N' end isjobsohio
  ,case when secondtier='√' then 'Y' else 'N' end issecondtier
  ,case when critical='√' then 'Y' else 'N' end iscritical
from oh_topjobs_import
where 1=1
and refno is not null
;
select * from oh_topjobs_load_vw order by 1;


drop table oh_topjobs;
create table oh_topjobs (
  publishdate date
  ,soccode varchar2(7) -- Standard Occupational Classification code - Bureau of Labor and Statistics (BLS)
  ,jobtitle varchar2(150)
  ,jobcategory varchar2(100)
  ,educationlevel varchar2(40)
  ,ojttraining varchar2(40)
  ,relevantworkexperience varchar2(25)
  ,startingwage int
  ,medianwage int
  ,annualopenings int
  ,annualgrowth int
  ,isbase varchar2(3)
  ,isjobsohio varchar2(3)
  ,issecondtier varchar2(3)
  ,iscritical varchar2(3)
  ,primary key (publishdate, jobtitle)
);
truncate table oh_topjobs;
insert into oh_topjobs select * from oh_topjobs_load_vw t;
commit;
select * from oh_topjobs order by 1;


-- corrected topjobs view
create or replace view oh_topjobs_base_vw as
-- Ohio top jobs not in the cross reference list (these need no correction)
select 
  publishdate
  ,soccode
  ,jobtitle
  ,jobcategory
  ,t.educationlevel
  ,ojttraining
  ,relevantworkexperience
  ,startingwage
  ,medianwage
  ,annualopenings
  ,annualgrowth
  ,isbase
  ,isjobsohio
  ,issecondtier
  ,iscritical
from oh_topjobs t 
where t.soccode not in (select otj_soccode from oh_topjobs_xref_vw)
UNION
-- Ohio top jobs in the cross reference list which need corrected codes and titles
select t.publishdate 
  --,t.soccode, t.jobtitle 
  ,x.soccode, b.soctitle jobtitle
  ,t.jobcategory, t.educationlevel, t.ojttraining, t.relevantworkexperience 
  ,t.startingwage, t.medianwage, t.annualopenings, t.annualgrowth
  ,isbase, isjobsohio, issecondtier, iscritical
from oh_topjobs t
join oh_topjobs_xref_vw x on x.otj_soccode = t.soccode
join bls_soc_vw b on b.soccode = x.soccode
;
select * from oh_topjobs_base_vw;



create or replace view oh_topjobs_vw as
select 
  t.publishdate
  ,i.publishdate soccip_pubishdate
  ,t.soccode
  ,i.cipcode
  ,t.jobtitle
  ,t.jobcategory
  ,e.ipeds_awardlevel_code
  ,t.educationlevel
  ,t.ojttraining
  ,t.relevantworkexperience
  ,t.startingwage
  ,t.medianwage
  ,t.annualopenings
  ,t.annualgrowth
  ,t.isbase
  ,t.isjobsohio
  ,t.issecondtier
  ,t.iscritical
from oh_topjobs_base_vw t
left join ipeds_cip_soc i on t.soccode = i.soccode and i.publishdate = date '2020-01-01' -- latest publish date
left join oh_topjobs_edulevel_vw e on e.educationlevel = t.educationlevel
;
select * from oh_topjobs_vw; -- repeats on doctoral award levels


-- any top job soc codes not in BLS base list? -- No
select * from oh_topjobs_base_vw t
left join bls_soc_base_vw s on s.soccode = t.soccode
where  s.soccode is null;

-- any top job soc codes not in BLS current list? -- Yes, fix by including these in oh_topjobs_xref_vw
select * from oh_topjobs_base_vw t
left join bls_soc_vw s on s.soccode = t.soccode
where  s.soccode is null
order by t.soccode
;



